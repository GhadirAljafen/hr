<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeRoleTypeToEnum extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

        public function up()
    {
        \Illuminate\Support\Facades\DB::statement(
            "ALTER TABLE users MODIFY COLUMN role ENUM('Manager', 'Employee') NOT NULL DEFAULT 'Employee'");
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('role')->default('Employee')->change();
        });
    }
}
