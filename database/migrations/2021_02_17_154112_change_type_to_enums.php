<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeTypeToEnums extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        \Illuminate\Support\Facades\DB::statement(
            "ALTER TABLE vacations CHANGE COLUMN type type ENUM('annual', 'sick', 'exceptional') NOT NULL DEFAULT 'annual'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vacations', function (Blueprint $table) {
            $table->string('type')->change();
        });
    }
}
