<?php

namespace App\Http\Controllers;

use App\Article;
use App\Vacation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class VacationsController extends Controller
{
    public function __construct()
    {
        //   $this->middleware('auth');
        $this->middleware('checkRole')->only('edit', 'update');
    }

    public function create()
    {
        //$vacationTypes = ['annual' => 'annual', 'sick' => 'sick', 'exceptional' => 'exceptional'];
        //compact($vacationTypes)
        return View('vacations.create' );
    }

    public function store(Request $request)
    {
        $user = Auth::user();
        $request->validate([
            'title' => 'required|max:50',
            'type' => 'required',
            'description' => 'required|min:30',
            'start_date' => 'required',
            'end_date' => 'required',
            'attachment' => 'mimes:pdf,xlx,csv|max:2048',

        ]);
        if (request()->hasFile($request['attachment'])) {
            $fileName = time() . '.' . $request->attachment->extension();
            $request->attachment->move(public_path('uploads'), $fileName);
            $vacation = ['title' => $request['title'],
                'type' => $request['type'],
                'description' => $request['description'],
                'attachment' => $fileName,
                'start_date' => $request['start_date'],
                'end_date' => $request['end_date']
            ];
            $user->vacations()->create($vacation);
        } else $user->vacations()->create($request->all());

        //route
        return redirect()->to('employee/vacation');
    }

//    public function fileUploadPost(Request $request)
//    {
////        $request->validate([
////            'file' => 'required|mimes:pdf,xlx,csv|max:2048',
////        ]);
//
//        $fileName = time().'.'.$request->file->extension();
//
//        $request->file->move(public_path('uploads'), $fileName);
//
//        return back()
//            ->with('success','You have successfully upload file.')
//            ->with('file',$fileName);
//
//    }

    /**
     * Display the specified resource.
     *
     * @param \App\Vacation $vacation
     * @return \Illuminate\Http\Response
     */
    public function edit(Vacation $vacation)
    {

        return View('vacations.edit', compact('vacation'));

    }

    /**
     * Display the specified resource.
     *
     * @param \App\Vacation $vacation
     * @return \Illuminate\Http\Response
     */
    public function show(Vacation $vacation)
    {
        $user_role = Auth::user()->role;
        return View('vacations.show', compact('vacation', 'user_role'));
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Vacation $vacation
     * @return \Illuminate\Http\Response
     */
    public function destroy(Vacation $vacation)
    {
        if (Auth::id() != $vacation->user_id) {
            return abort(401);
        }
        $vacation->delete();
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Vacation $vacation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Vacation $vacation)
    {
        $vacation->update($request->all());
        return redirect()->back();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

}
