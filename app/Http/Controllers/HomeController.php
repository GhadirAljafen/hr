<?php
namespace App\Http\Controllers;
use App\Vacation;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        // to return all user's vacations
        $user_role = Auth::user()->role;
        if($user_role!='Manager'){
            $vacations = Auth::user()->vacations;
            return view('home',compact('vacations','user_role'));
        }
        else {
            $vacations = Vacation::all();
            return view('home',compact('vacations','user_role'));
        }

    }
    public  function employeeVacations(){
        $user_role = Auth::user()->role;
            $vacations = Auth::user()->vacations;

            return view('vacations/employee_vacations',compact('vacations','user_role'));

    }

    public  function managerVacations(){
        $user_role = Auth::user()->role;
        $vacations = Vacation::orderBy('id','DESC')->get();

        return view('vacations/managerView',compact('vacations','user_role'));

    }
}
