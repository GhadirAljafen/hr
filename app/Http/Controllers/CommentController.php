<?php

namespace App\Http\Controllers;

use App\Vacation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CommentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function store(Request $request, Vacation $vacation)
    {
        //get the user id from session
        $request['user_id'] = Auth::id();
        //$request ['vacations_id'] = $request->vacation->id;
//        dd($vacation->id);
        $vacation->comments()->create($request->all());
        return redirect()->back();

    }
}
