<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $fillable = [
        'user_id',
        'vacation_id',
        'content'
    ];

    public function vacation()
    {
        return $this->belongsTo(Vacations::class, 'vacation_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
