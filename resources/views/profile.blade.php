@extends('layouts.app')
@section('content')
    <style>
        .card {
            margin: 0 auto;
            float: none;
            margin-bottom: 10px;
        }
    </style>

    <div class="card col-6">
        <div class=" m-3 " style="font-size: 44px; text-align:center;">
            <i class="fa fa-user-circle-o fa-5x" style="color:lightgray;"></i>
        </div>
        <form action={{route('user.update',$user)}} method="post">
            @csrf
            @method('PUT')
            <div class="form-group p-3">
                <label class="font-weight-bolder mt-2 " for='name'>{{__('Name')}}</label>
                <input class="form-control pt-2" name="name" id="name" type="text" value="{{$user->name}}">

                <label class="font-weight-bold mt-2" for='email'>{{__('E-Mail Address')}}</label>
                <input class="form-control " name="email" id="email" type="email" value="{{$user->email}}">

                <label class="font-weight-bold mt-2" for='job_title'>{{__('Job Title')}}</label>
                <input class="form-control" name="job_title" id="job_title" type="text"
                       value="{{$user->job_title}}">

                <label class="font-weight-bold mt-2" for='DoB'>{{__('Date of Birth')}}</label>
                <input class="form-control" name="DoB" id="DoB" type="date" value="{{$user->DoB}}">

            </div>
            <div class="form-group p-3">
                <input class="form-control btn btn-outline-success bt-lg" type="submit" name="submit"
                       value="{{__('Save')}}"/>
            </div>
        </form>

    </div>

@endsection
