<!doctype html>
<html lang="{{config('app.locale')}}" dir="{{config('app.direction')}}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Laravel') }}</title>
    {{--        @if(config('app.direction') === 'rtl')--}}
    {{--            {{config('app.name_ar')}}--}}
    {{--        @else--}}
    {{--        {{config('app.name')}}--}}
    {{--        @endif--}}
    <link rel="icon" href="{!! asset('img/hr_logo.png') !!}"/>
    <link rel="stylesheet" href="https://cdn.rtlcss.com/bootstrap/v4.5.3/css/bootstrap.min.css" integrity="sha384-JvExCACAZcHNJEc7156QaHXTnQL3hQBixvj5RV5buE7vgnNEzzskDtx9NQ4p6BJe" crossorigin="anonymous">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Cairo:wght@300&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('font-awesome-4.7.0/css/font-awesome.css')}}">

<style>
    /*html,*/
    /*body {*/
    /*    height: 100%;*/
    /*    margin: 0;*/
    /*    padding: 0;*/
    /*}*/
     body{
         font-family: 'Cairo', sans-serif;
     }

    /*main {*/
    /*    display: flex;*/
    /*    flex-direction: column;*/
    /*    min-height: 100%;*/
    /*}*/
    footer.stikybottom {
        position: fixed;
        bottom:0;
        left: 0;
        right:0;
    }
</style>
    <!-- Scripts -->
{{--    <script src="{{ asset('js/app.js') }}" defer></script>--}}
    <script src="/app.js"></script>


    {{--    <!-- Fonts -->--}}
{{--    <link rel="dns-prefetch" href="//fonts.gstatic.com">--}}
{{--    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">--}}

    <!-- Styles -->

{{--    <link type="text/css" rel="stylesheet" href="{{ mix('css/app.css') }}">--}}
{{--    <link href="{{ asset('css/app.css') }}" rel="stylesheet">--}}




</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}"> <img src="{{ URL::to('/img/hr_logo.png') }}" style="width: 40px; height: 40px;">
                    <b style="color: #1965b6">{{ config('app.name_ar', 'HR') }}</b>
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    @auth
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item">
{{--                            <a class="text-secondary pr-2 nav-link"  @if($user_role=='Manager') href="{{route('allVacations')}}" @else href="{{route('employeeVacations')}}"  @endif > <i class="fa fa-list-alt" aria-hidden="true"></i> {{__('الإجازات')}}</a>--}}
                        </li>
{{--                        <li class="nav-item">--}}
{{--                            <a class="pr-2 nav-link" href="{{route('user.show')}}" style="color: #ed7d2a">  <i class="fa fa-user pr-1" aria-hidden="true"></i>{{__('Profile')}}</a>--}}
{{--                        </li>--}}
                    </ul>
                @endauth

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else

                            <li class="nav-item">
                                <a class=" pr-2" href="{{route('user.show')}}" style="color: #ed7d2a">
                                  {{__('Welcome back')}} {{ Auth::user()->name }}
                                </a>

                            <li class="nav-item">
                                <a class=" btn btn-secondary " href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    <i class="fa fa-sign-out" aria-hidden="true"></i> {{ __('Logout') }}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                    @csrf
                                </form>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>


        <div id="big-container">
        <main  id="main" class="py-4 container wrapper">
            @if($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @yield('content')
        </main>

    <footer id="footer" class="footer bg-light text-center text-lg-start">
        <!-- Grid container -->
        <div class="container">
                <div class="text-center  mb-4 mb-md-0">
                    <h5 class="text-uppercase"> <img src="{{ URL::to('/img/hr_logo.png') }}" style="width: 40px; height: 40px;"> <b style="color: #1965b6">{{ config('app.name_ar', 'HR') }}</b> </h5>
                    <p>
                        {{__('The human resource management system in the Kingdom of Saudi Arabia, designed together locally compliant with the labor law, it is smoothly effective, and easy to use.')}}
                    </p>
                </div>
        </div>
        <!-- Copyright -->
        <div class="text-center p-3" style="background-color: #eaf3fae0">
            © 2021 {{__('Copyright')}}:
            <a class="text-dark"> HR</a>
        </div>
        <!-- Copyright -->
    </footer>
    </div>
    </div>



<script>
    let maxHeight = Math.max(
      document.body.scrollHeight , document.documentElement.scrollHeight,
        document.body.offsetHeight, document.documentElement.offsetHeight,
        document.body.clientHeight, document.documentElement.clientHeight
    );

    document.addEventListener("DOMContentLoaded", function () {
       let element = document.getElementById('app');
        let height = element.offsetHeight;
        // console.log('MAX '+ maxHeight);
        // console.log('Element height'+height);
        // console.log('Screen height:'+screen.height);
        if (height < maxHeight) {
            document.getElementById("footer").classList.add('stikybottom');
        }
    }, false);
</script>
</body>
</html>
