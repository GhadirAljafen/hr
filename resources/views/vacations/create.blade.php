@extends('layouts.app')
@section('title',__('New Vacation'))
@section('content')
   <h2>{{__('Request New Vacation')}}</h2>
   <form action="{{route('vacations.store')}}" method="post" enctype="multipart/form-data">
       @csrf
       <div class="form-group">
           <label for="title">{{__('Title')}}</label>
           <input class="form-control col-6" type="text" name="title">
       </div>
       <div class="form-group">
           <div>
               <label>{{__('Request category')}}</label>
           </div>

           <input type="radio" id="type1" name="type" value="0">
           <label for="type1"> {{__('Annual')}}</label>
           <input type="radio" id="type3" name="type" value="1">
           <label for="type3"> {{__('Sick')}}</label>
           <input type="radio" id="type4" name="type" value="2">
           <label for="type4"> {{__('Exceptional')}}</label>


       </div>
       <div class="form-group">
           <label for="start_date">{{__('Start Date')}}</label>
           <input type="date" class="form-control col-6"  id="start_date" name="start_date">
           <label for="end_date">{{__('End Date')}}</label>
           <input class="form-control col-6" type="date" id="start_date" name="end_date">
       </div>

       <div class="form-group ">
           <label for="description">{{__('Description')}}</label>
           <textarea name="description" id="description" rows="10" cols="30" class="form-control col-6"></textarea>
       </div>

       <div class="form-group">
           <label for="attachment">{{__('Select a file (optional)')}}</label>
           <input class="form-control col-6" type="file" id="attachment" name="attachment">
       </div>
       <div class="form-group">
           <button class="form-control btn btn-success col-6" type="submit">{{__('Send')}}</button>
       </div>

   </form>
    @endsection
