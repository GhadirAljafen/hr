@extends('layouts/app')
<style>
    .badge {
        position: absolute;
        top: 10px;
        left: 0;
        margin-left: 5px;
        width: auto;
    }

</style>
@section('content')

    @if($user_role!='Manager')
            <div>
                <a href="{{route('vacations.create')}}" class="btn btn-sm btn-primary mb-4">{{__('New Request')}} <i class="fa fa-plus"></i></a>

                <h4>{{__('Your requests')}}</h4>
            </div>
        <div class="row" id="vacations">
    @forelse($vacations as $vacation)
        <div class="col-md-3">
            <div class="card mb-2">
                <div class="card-body">
                    <a href="{{route('vacations.show',$vacation)}}"> {{$vacation->title}}</a>
                    <span id="badge" class="status badge
@if($vacation->status === 'pending'|$vacation->status === 'معلق')
                        badge-info
                        @elseif($vacation->status === 'approved'|$vacation->status === 'مقبول')
                    badge-success
                    @elseif($vacation->status ==='rejected' |$vacation->status === 'مرفوض')
                        badge-danger
@endif
">{{__($vacation->status)}}</span>
                </div>
                <div class="card-footer ">
                    <form method="post" action="{{route('vacations.destroy',$vacation)}}"
                          style="">
                        @method('DELETE')
                        @csrf
                        <button class="btn btn-outline-danger btn-sm col-12" id="btn-delete"
                                onclick="return confirm('{{__('Are you sure?')}}')">{{__('Delete')}} <i class="fa fa-trash"></i></button>
                    </form>
                </div>
            </div>
        </div>
            @empty
        <p>{{ __('You don\'t have any requests yet') }}</p>
    @endforelse
        </div></div>


            @endif
@endsection
