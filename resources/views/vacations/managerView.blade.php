@extends('layouts.app')
@section('content')
    <link href="https://cdn.datatables.net/1.10.23/css/dataTables.bootstrap4.min.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/rowreorder/1.2.7/css/rowReorder.dataTables.min.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/responsive/2.2.7/css/responsive.dataTables.min.css" rel="stylesheet">

    <div>
        <h4 class="text-info p-3"><b>{{__('Employee\'s requests')}}</b></h4>
    </div>
    <table class="table table-hover" id="myTable">
        <thead>
        <tr class="table-info">
            <th>{{__('Title')}}</th>
            <th>{{__('Status')}}</th>
            <th>{{__('Type')}}</th>
            <th>{{__('From')}}/{{__('to')}}</th>
            <th>{{__('Employee')}}</th>
            <th>{{__('File')}}</th>
            <th></th>
            {{--        <th>{{__('Edit the status')}}</th>--}}
        </tr>
        </thead>
        @forelse($vacations as $vacation)
            <tr>
                <th> {{$vacation->title}}</a> </th>
                <th class="@if($vacation->status==='approved')text-success
                        @elseif($vacation->status==='rejected')text-danger
                          @elseif($vacation->status==='pending')text-info
                        @endif"> {{__($vacation->status)}}</th>
                <th>{{__($vacation->type)}}</th>
                <th>{{__('From')}} : {{$vacation->start_date}} {{__('to')}} : {{$vacation->end_date}} </th>
                <th> {{$vacation->user->name}}</th>
                <th>  @isset($vacation->attachment)<a
                        href='{{ asset("uploads/$vacation->attachment") }}' target="_blank">{{$vacation->attachment}} </a>@endisset
                </th>
                <th><a href="{{route('vacations.show',$vacation)}}"
                       class="btn btn-outline-info btn-sm" id="edit_btn"> <i class="fa fa-info" aria-hidden="true"></i></a>
                </th>

            </tr>


        @empty
            {{__('No requests yet')}}
        @endforelse
    </table>


@endsection
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="//cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="https://cdn.datatables.net/1.10.23/js/dataTables.bootstrap4.min.js"></script>
<script src="https://cdn.datatables.net/rowreorder/1.2.7/js/dataTables.rowReorder.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.7/js/dataTables.responsive.min.js"></script>

<script>
    $(document).ready(function () {
        $.noConflict();
        $('#myTable').DataTable({
            "serverSide": false,
            'sort': true,
            "processing": false,
            "searching": false,
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.22/i18n/Arabic.json"
            },
            rowReorder: {
                selector: 'td:nth-child(2)'
            },
            responsive: true

        });

    });

</script>
