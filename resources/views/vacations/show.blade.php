@extends('layouts.app')
@section('title',$vacation->title)
@section('content')
    <style>

    </style>
    <div class="card">
        <h3 class="card-header alert-info">
            {{$vacation->title}}
        </h3>
        <div class="card-body">
            <p><b>{{__('Description')}} </b></p>
            <p>{!! nl2br($vacation->description) !!}</p>
            <p><b>{{__('Request category')}}:</b> {!! __(nl2br($vacation->type))!!}</p>
            <p><b>{{__('From')}}:</b> {!! nl2br($vacation->start_date) !!} <b>{{__('to')}}
                    :</b> {!! nl2br($vacation->end_date) !!}</p>
            <p id="status"><b>{{__('Request status')}}:</b> {!! nl2br(__($vacation->status)) !!}</p>
            <p>@isset($vacation->attachment) {{__('Attachment')}} : <a
                    href='{{ asset("uploads/$vacation->attachment")}}' target="_blank">{{$vacation->attachment}}</a>@endisset</p>

            @if($user_role=='Manager')
                <a id="edit" class="text-link" style="color: #ed7d2a" >{{__('Edit')}} <i
                        class="fa fa-pencil"></i></a>
                <div id="edit-form" style="display: none">
                    <form action="{{route('vacations.update',$vacation)}}" method="post">
                        @csrf
                        @method('PUT')
                        <div class="form-group p-3">
                            <select class=" form-control col-4 pt-1 pb-1" name="status" id="status">
                                <option value="pending">{{__('Pend')}}</option>
                                <option value="approved">{{__('Approve')}}</option>
                                <option value="rejected">{{__('Reject')}}</option>
                            </select>

                            <button id="btn-edit" type="submit" class="btn btn-success text-light m-3"> {{__('Edit')}}
                                <i class="fa fa-pencil"></i></button>
                        </div>
                    </form>
                </div>
            @endif
            {{--             here will be a link to open attachments--}}
        </div>
        <div class="dropdown-divider mt-3 "></div>
        <div class="article-footer  ml-3 mb-3">
            <div><b>{{__('Employee')}}:</b> {{$vacation->user->name}}</div>
            <div><b>{{__('Created at')}}:</b> {{$vacation->created_at}}</div>
            <div><b>{{__('Updated at')}}:</b> {{$vacation->updated_at}}</div>
        </div>


    </div>

    <div id="comments" class="mt-4">
        <h5 class="card-header alert-info">{{__('Comments')}}</h5>

        @forelse($vacation->comments as $comment)
            <div class="card p-3 mt-2">
                {{$comment->content}}
                <div class="dropdown-divider mt-3"></div>
                <p>{{__('Author')}}:{{$comment->user->name}}</p>


            </div>
        @empty

            {{__('No comments yet')}}
        @endforelse
    </div>
    @auth
        <div id="comment-form " class="mt-5">
            <div class="card">
                <h5 class="card-header alert-secondary">{{__('Type your comment here')}}</h5>
                <div class="card-body">
                    <form action="{{route('comments.store',$vacation->id)}}" method="post">
                        @csrf
                        <div class="form-group">
                            <label for="content">{{__('Content')}}</label>
                            <textarea
                                class="form-control"
                                placeholder="{{__('Type your comment here')}}"
                                name="content"
                                id="content"
                                cols="30"
                                rows="10"></textarea>
                        </div>
                        <div class="form-group">
                            <button class="btn btn-success" type="submit">{{__('Save')}}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    @else
        <p class="text-secondary mt-3"><a href="{{route('login')}}">{{__('Login to post a comments')}}</a></p>
    @endauth
@endsection
<script src="/JS/jquery-3.5.1.js"></script>
<script>
  //  use jQuery and toggle to do this
  //
  //   function showForm() {
  //       var x = document.getElementById("edit-form");
  //       if (x.style.display === "none") {
  //           x.style.display = "block";
  //       } else {
  //           x.style.display = "none";
  //       }
  //   }
        $(document).ready(function () {
            $('#edit').click(function () {
                $('#edit-form').toggle();
            });
        });

</script>

