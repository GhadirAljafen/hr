@extends('layouts.app')
@section('title',__('New Vacation'))
@section('content')
    <h2>{{__('Edit Vacation\'s Status')}}</h2>
    <form action="{{route('vacations.update',$vacation)}}" method="post">
        @method('PUT')
        @csrf
        <div class="form-group">
            <label for="title">{{__('Title')}}</label>
            <input class="form-control col-6" type="text" name="title" @isset($vacation) disabled value="{{__($vacation->title)}}" @endisset>
        </div>
        <div class="form-group">
            <div>
                <label>{{__('Request category')}}</label>
            </div>

            <input type="radio" id="type1" name="type" value="{{__('Annual')}}"  @isset($vacation) disabled checked @endisset">
            <label for="type1"> {{__('Annual')}}</label>
            <input type="radio" id="type3" name="type" value="{{__('Sick')}}" @isset($vacation) disabled checked @endisset">
            <label for="type3"> {{__('Sick')}}</label>
            <input type="radio" id="type4" name="type" value="{{__('Exceptional')}}" @isset($vacation) disabled checked @endisset">
            <label for="type4"> {{__('exceptional')}}</label>
        </div>
        <div class="form-group">
            <label for="start_date">{{__('Start Date')}}</label>
            <input type="date" class="form-control col-6"  id="start_date" name="start_date"  @isset($vacation) disabled value="{{__($vacation->start_date)}}" @endisset>
            <label for="end_date">{{__('End Date')}}</label>
            <input class="form-control col-6" type="date" id="end_date" name="end_date" @isset($vacation) disabled value="{{__($vacation->end_date)}}" @endisset>
        </div>

        <div class="form-group ">
            <label for="description">{{__('Description')}}</label>
            <textarea name="description" id="description" rows="10" cols="30" class="form-control col-6" @isset($vacation) disabled @endisset>@isset($vacation) {{__($vacation->description)}} @endisset</textarea>
        </div>
{{--        <div class="form-group">--}}
{{--            <label for="attachment">{{__('Select a file (optional)')}}</label>--}}
{{--            <input class="form-control col-6" type="file" id="attachment" name="attachment">--}}
{{--        </div>--}}
        <div class="form-group">
            <select class=" form-control col-6" name="status" id="status">
                <option value="pending">{{__('Pend')}}</option>
                <option value="approved">{{__('Approve')}}</option>
                <option value="rejected">{{__('Reject')}}</option>
            </select>
        </div>
        <div class="form-group">
            <button class="form-control btn btn-success col-6" type="submit">{{__('Edit')}}</button>
        </div>

    </form>
@endsection
