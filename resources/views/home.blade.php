@extends('layouts.app')
@section('content')
    <style>
        {{--@media (max-width: 760px) {--}}
        {{--    .jumbotron {--}}
        {{--        background-image: url("{{URL::asset('img/HR_BG.jpg')}}");--}}
        {{--        background-size: 100%;--}}
        {{--        height: 600px;--}}
        {{--        /*background-size: cover;*/--}}
        {{--        background-repeat: no-repeat;--}}
        {{--        position: relative;--}}
        {{--        background-position: center;--}}
        {{--    }--}}

        {{--}--}}
        .jumbotron {
            background-image: url("{{URL::asset('img/HR_BG.jpg')}}");
            background-size: 100%;
            height: 600px;
            /*background-size: cover;*/
            background-repeat: no-repeat;
            position: relative;
            background-position: center;
        }

        .img-text {
            position: absolute;
            border-radius: 40px;
            bottom: 0px;
            width: 95%;
            padding-top: 5px;
            /* height: 50px; */
            margin-top: 40px;
            color: #1965b6;
            text-align: center;
            font-size: 30px;
            background-color: #eaf3fae0;
        }
    </style>



    @if($user_role!='Manager')

        <div class="jumbotron ">
            <div class="img-text">
                <h3> {{__('Welcome to HR system')}}</h3>
                <p class="lead">{{__('By using HR system you can keep track of your requests easily anytime anywhere')}}</p>
                <a href="{{route('vacations.create')}}" class="btn btn-sm btn-primary mb-4">{{__('New Request')}}</a>
                <a href="{{route('employeeVacations')}}"
                   class="btn btn-sm btn-secondary mb-4">{{__('Your requests')}}</a>
            </div>
        </div>
    @endif
    <div>
        @if($user_role=='Manager')

            <div class="jumbotron">
                <div class="img-text">
                    <h3>{{__('Welcome to HR system')}}</h3>
                    <p class="lead">{{__('By using HR system you can manage your organization anytime anywhere')}}</p>
                    <a href="{{route('allVacations')}}" class="btn btn-sm btn-primary mb-4">{{__('All Requests')}}</a>

                </div>
            </div>
        @endif
    </div>
    <script>
        // let vacations = document.getElementById('vacations');
        // let statuses = document.getElementsByClassName('status');
        // for(let s of statuses){
        //     if(s.innerHTML === 'pending'){
        //         s.classList.remove('badge-danger')
        //         s.classList.add('badge-primary')
        //     }
        // }
        // for(let status in statuses){
        //     console.log(status.innerHTML)
        // }
        // console.log(vacations, status)
        // alertify.confirm("This is a confirm dialog.",
        //     function(){
        //         alertify.success('Ok');
        //     },
        //     function(){
        //         alertify.error('Cancel');
        //     });
        {{--let status = {!! json_encode($vacation->status,JSON_HEX_TAG) !!};--}}

        {{--if(status=='pending'){--}}
        {{--    document.getElementById('badge').className='badge badge-info';--}}

        {{--}--}}
        {{--if(status=='approved'){--}}
        {{--    document.getElementById('badge').className='badge badge-success';--}}
        {{--}--}}

        {{--if(status=='rejected'){--}}
        {{--    document.getElementById('badge').className='badge badge-danger';--}}
        {{--}--}}
        {{--    if ({!! json_encode(Auth::user()->role != 'Manager')!!}) {--}}
        {{--        let btn_edit = document.getElementById('edit_btn');--}}
        {{--         btn_edit.style.display = "none";--}}
        {{--    }--}}


    </script>

@endsection
