<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/', 'HomeController@index')->name('home');
Route::resource('/vacations','VacationsController');
Route::get('/create','VacationsController@create')->name('create');
Route::post('/vacations/store','VacationsController@store')->name('vacations.store');
//Route::post('/vacations/update','VacationsController@update')->name('vacations.update');
Route::get('/unauthorized',function (){
    return "Unauthorized request";
});
Route::post('/comment/{vacation}','CommentController@store')->name('comments.store');
Route::get('/test/env', function () {
    dd(env('DB_DATABASE')); // dump db variable value one by one
});

Route::get('employee/vacation','HomeController@employeeVacations')->name('employeeVacations');
Route::get('manager/vacation','HomeController@managerVacations')->name('allVacations');

//Route::resource('/user','UserController');

Route::get('/profile','UserController@show')->name('user.show');
//SQL injection
Route::put('profile/profile/{user}','UserController@update')->name('user.update');



